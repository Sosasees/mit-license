# sosasees' copy of the MIT License

my personal copy of the MIT License,
which replaces the _copyright holders_ field with _sosasees_.

[View the License file](LICENSE).
